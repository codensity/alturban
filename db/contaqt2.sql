-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.13-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for alturban
DROP DATABASE IF EXISTS `alturban`;
CREATE DATABASE IF NOT EXISTS `alturban` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `alturban`;


-- Dumping structure for table alturban.al_article
DROP TABLE IF EXISTS `al_article`;
CREATE TABLE IF NOT EXISTS `al_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `view` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `thumbnail_base_url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail_path` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `updater_id` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `published_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article_author` (`author_id`),
  KEY `fk_article_updater` (`updater_id`),
  KEY `fk_article_category` (`category_id`),
  CONSTRAINT `fk_article_author` FOREIGN KEY (`author_id`) REFERENCES `al_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_article_category` FOREIGN KEY (`category_id`) REFERENCES `al_article_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_article_updater` FOREIGN KEY (`updater_id`) REFERENCES `al_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_article: ~0 rows (approximately)
DELETE FROM `al_article`;
/*!40000 ALTER TABLE `al_article` DISABLE KEYS */;
/*!40000 ALTER TABLE `al_article` ENABLE KEYS */;


-- Dumping structure for table alturban.al_article_attachment
DROP TABLE IF EXISTS `al_article_attachment`;
CREATE TABLE IF NOT EXISTS `al_article_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `base_url` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article_attachment_article` (`article_id`),
  CONSTRAINT `fk_article_attachment_article` FOREIGN KEY (`article_id`) REFERENCES `al_article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table alturban.al_article_attachment: ~0 rows (approximately)
DELETE FROM `al_article_attachment`;
/*!40000 ALTER TABLE `al_article_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `al_article_attachment` ENABLE KEYS */;


-- Dumping structure for table alturban.al_article_category
DROP TABLE IF EXISTS `al_article_category`;
CREATE TABLE IF NOT EXISTS `al_article_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `parent_id` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article_category_section` (`parent_id`),
  CONSTRAINT `fk_article_category_section` FOREIGN KEY (`parent_id`) REFERENCES `al_article_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_article_category: ~1 rows (approximately)
DELETE FROM `al_article_category`;
/*!40000 ALTER TABLE `al_article_category` DISABLE KEYS */;
INSERT INTO `al_article_category` (`id`, `slug`, `title`, `body`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'news', 'News', NULL, NULL, 1, 1466407667, NULL);
/*!40000 ALTER TABLE `al_article_category` ENABLE KEYS */;


-- Dumping structure for table alturban.al_file_storage_item
DROP TABLE IF EXISTS `al_file_storage_item`;
CREATE TABLE IF NOT EXISTS `al_file_storage_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component` varchar(255) NOT NULL,
  `base_url` varchar(1024) NOT NULL,
  `path` varchar(1024) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `upload_ip` varchar(15) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table alturban.al_file_storage_item: ~0 rows (approximately)
DELETE FROM `al_file_storage_item`;
/*!40000 ALTER TABLE `al_file_storage_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `al_file_storage_item` ENABLE KEYS */;


-- Dumping structure for table alturban.al_i18n_message
DROP TABLE IF EXISTS `al_i18n_message`;
CREATE TABLE IF NOT EXISTS `al_i18n_message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`,`language`),
  CONSTRAINT `fk_i18n_message_source_message` FOREIGN KEY (`id`) REFERENCES `al_i18n_source_message` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_i18n_message: ~0 rows (approximately)
DELETE FROM `al_i18n_message`;
/*!40000 ALTER TABLE `al_i18n_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `al_i18n_message` ENABLE KEYS */;


-- Dumping structure for table alturban.al_i18n_source_message
DROP TABLE IF EXISTS `al_i18n_source_message`;
CREATE TABLE IF NOT EXISTS `al_i18n_source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_i18n_source_message: ~0 rows (approximately)
DELETE FROM `al_i18n_source_message`;
/*!40000 ALTER TABLE `al_i18n_source_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `al_i18n_source_message` ENABLE KEYS */;


-- Dumping structure for table alturban.al_key_storage_item
DROP TABLE IF EXISTS `al_key_storage_item`;
CREATE TABLE IF NOT EXISTS `al_key_storage_item` (
  `key` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`key`),
  UNIQUE KEY `idx_key_storage_item_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_key_storage_item: ~5 rows (approximately)
DELETE FROM `al_key_storage_item`;
/*!40000 ALTER TABLE `al_key_storage_item` DISABLE KEYS */;
INSERT INTO `al_key_storage_item` (`key`, `value`, `comment`, `updated_at`, `created_at`) VALUES
	('backend.layout-boxed', '1', NULL, 1466407888, NULL),
	('backend.layout-collapsed-sidebar', '0', NULL, 1466407888, NULL),
	('backend.layout-fixed', '0', NULL, 1466407888, NULL),
	('backend.theme-skin', 'skin-green', 'skin-blue, skin-black, skin-purple, skin-green, skin-red, skin-yellow', 1466407888, NULL),
	('frontend.maintenance', 'disabled', 'Set it to "true" to turn on maintenance mode', 1466407888, NULL);
/*!40000 ALTER TABLE `al_key_storage_item` ENABLE KEYS */;


-- Dumping structure for table alturban.al_page
DROP TABLE IF EXISTS `al_page`;
CREATE TABLE IF NOT EXISTS `al_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `view` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_page: ~1 rows (approximately)
DELETE FROM `al_page`;
/*!40000 ALTER TABLE `al_page` DISABLE KEYS */;
INSERT INTO `al_page` (`id`, `slug`, `title`, `body`, `view`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'about', 'About', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', NULL, 1, 1466407667, 1466407667);
/*!40000 ALTER TABLE `al_page` ENABLE KEYS */;


-- Dumping structure for table alturban.al_rbac_auth_assignment
DROP TABLE IF EXISTS `al_rbac_auth_assignment`;
CREATE TABLE IF NOT EXISTS `al_rbac_auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `al_rbac_auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `al_rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_rbac_auth_assignment: ~3 rows (approximately)
DELETE FROM `al_rbac_auth_assignment`;
/*!40000 ALTER TABLE `al_rbac_auth_assignment` DISABLE KEYS */;
INSERT INTO `al_rbac_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
	('administrator', '1', 1466407671),
	('manager', '2', 1466407671),
	('user', '3', 1466407671);
/*!40000 ALTER TABLE `al_rbac_auth_assignment` ENABLE KEYS */;


-- Dumping structure for table alturban.al_rbac_auth_item
DROP TABLE IF EXISTS `al_rbac_auth_item`;
CREATE TABLE IF NOT EXISTS `al_rbac_auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `al_rbac_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `al_rbac_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_rbac_auth_item: ~4 rows (approximately)
DELETE FROM `al_rbac_auth_item`;
/*!40000 ALTER TABLE `al_rbac_auth_item` DISABLE KEYS */;
INSERT INTO `al_rbac_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
	('/*', 2, NULL, NULL, NULL, 1466407815, 1466407815),
	('administrator', 1, NULL, NULL, NULL, 1466407671, 1466407671),
	('loginToBackend', 2, NULL, NULL, NULL, 1466407671, 1466407671),
	('manager', 1, NULL, NULL, NULL, 1466407671, 1466407671),
	('user', 1, NULL, NULL, NULL, 1466407671, 1466407671);
/*!40000 ALTER TABLE `al_rbac_auth_item` ENABLE KEYS */;


-- Dumping structure for table alturban.al_rbac_auth_item_child
DROP TABLE IF EXISTS `al_rbac_auth_item_child`;
CREATE TABLE IF NOT EXISTS `al_rbac_auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `al_rbac_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `al_rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `al_rbac_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `al_rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_rbac_auth_item_child: ~3 rows (approximately)
DELETE FROM `al_rbac_auth_item_child`;
/*!40000 ALTER TABLE `al_rbac_auth_item_child` DISABLE KEYS */;
INSERT INTO `al_rbac_auth_item_child` (`parent`, `child`) VALUES
	('administrator', '/*'),
	('administrator', 'manager'),
	('manager', 'loginToBackend'),
	('manager', 'user');
/*!40000 ALTER TABLE `al_rbac_auth_item_child` ENABLE KEYS */;


-- Dumping structure for table alturban.al_rbac_auth_rule
DROP TABLE IF EXISTS `al_rbac_auth_rule`;
CREATE TABLE IF NOT EXISTS `al_rbac_auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_rbac_auth_rule: ~0 rows (approximately)
DELETE FROM `al_rbac_auth_rule`;
/*!40000 ALTER TABLE `al_rbac_auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `al_rbac_auth_rule` ENABLE KEYS */;


-- Dumping structure for table alturban.al_system_db_migration
DROP TABLE IF EXISTS `al_system_db_migration`;
CREATE TABLE IF NOT EXISTS `al_system_db_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table alturban.al_system_db_migration: ~15 rows (approximately)
DELETE FROM `al_system_db_migration`;
/*!40000 ALTER TABLE `al_system_db_migration` DISABLE KEYS */;
INSERT INTO `al_system_db_migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1466407647),
	('m140703_123000_user', 1466407651),
	('m140703_123055_log', 1466407652),
	('m140703_123104_page', 1466407652),
	('m140703_123803_article', 1466407657),
	('m140703_123813_rbac', 1466407659),
	('m140709_173306_widget_menu', 1466407659),
	('m140709_173333_widget_text', 1466407660),
	('m140712_123329_widget_carousel', 1466407661),
	('m140805_084745_key_storage_item', 1466407662),
	('m141012_101932_i18n_tables', 1466407664),
	('m150318_213934_file_storage_item', 1466407664),
	('m150414_195800_timeline_event', 1466407665),
	('m150725_192740_seed_data', 1466407667),
	('m150929_074021_article_attachment_order', 1466407668);
/*!40000 ALTER TABLE `al_system_db_migration` ENABLE KEYS */;


-- Dumping structure for table alturban.al_system_log
DROP TABLE IF EXISTS `al_system_log`;
CREATE TABLE IF NOT EXISTS `al_system_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_time` double DEFAULT NULL,
  `prefix` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `idx_log_level` (`level`),
  KEY `idx_log_category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_system_log: ~1 rows (approximately)
DELETE FROM `al_system_log`;
/*!40000 ALTER TABLE `al_system_log` DISABLE KEYS */;
INSERT INTO `al_system_log` (`id`, `level`, `category`, `log_time`, `prefix`, `message`) VALUES
	(1, 1, 'yii\\base\\ErrorException:32', 1466407671.9011, '[console][]', 'exception \'yii\\base\\ErrorException\' with message \'Module \'openssl\' already loaded\' in Unknown:0\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}'),
	(2, 2, 'yii\\web\\User::loginByCookie', 1466407692.9023, '[frontend][/alturban/frontend/web/]', 'Invalid auth key attempted for user \'1\': 8o42KREvHLXVr9y6D4QWmfV3UfMJxTwb'),
	(3, 2, 'yii\\web\\User::loginByCookie', 1466407696.6021, '[frontend][/alturban/frontend/web/user/sign-in/login]', 'Invalid auth key attempted for user \'1\': 8o42KREvHLXVr9y6D4QWmfV3UfMJxTwb'),
	(4, 2, 'yii\\web\\User::loginByCookie', 1466407703.187, '[frontend][/alturban/frontend/web/user/sign-in/login]', 'Invalid auth key attempted for user \'1\': 8o42KREvHLXVr9y6D4QWmfV3UfMJxTwb'),
	(5, 1, 'yii\\i18n\\PhpMessageSource::loadMessages', 1466407935.0071, '[backend][/alturban/backend/web/sign-in/profile]', 'The message file for category \'filekit/widget\' does not exist: C:\\xampp\\htdocs\\alturban\\vendor\\trntv\\yii2-file-kit\\src\\widget/messages/en/filekit/widget.php');
/*!40000 ALTER TABLE `al_system_log` ENABLE KEYS */;


-- Dumping structure for table alturban.al_system_rbac_migration
DROP TABLE IF EXISTS `al_system_rbac_migration`;
CREATE TABLE IF NOT EXISTS `al_system_rbac_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table alturban.al_system_rbac_migration: ~3 rows (approximately)
DELETE FROM `al_system_rbac_migration`;
/*!40000 ALTER TABLE `al_system_rbac_migration` DISABLE KEYS */;
INSERT INTO `al_system_rbac_migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1466407668),
	('m150625_214101_roles', 1466407671),
	('m150625_215624_init_permissions', 1466407671);
/*!40000 ALTER TABLE `al_system_rbac_migration` ENABLE KEYS */;


-- Dumping structure for table alturban.al_timeline_event
DROP TABLE IF EXISTS `al_timeline_event`;
CREATE TABLE IF NOT EXISTS `al_timeline_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `event` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_created_at` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_timeline_event: ~3 rows (approximately)
DELETE FROM `al_timeline_event`;
/*!40000 ALTER TABLE `al_timeline_event` DISABLE KEYS */;
INSERT INTO `al_timeline_event` (`id`, `application`, `category`, `event`, `data`, `created_at`) VALUES
	(1, 'frontend', 'user', 'signup', '{"public_identity":"webmaster","user_id":1,"created_at":1466407665}', 1466407665),
	(2, 'frontend', 'user', 'signup', '{"public_identity":"manager","user_id":2,"created_at":1466407665}', 1466407665),
	(3, 'frontend', 'user', 'signup', '{"public_identity":"user","user_id":3,"created_at":1466407665}', 1466407665);
/*!40000 ALTER TABLE `al_timeline_event` ENABLE KEYS */;


-- Dumping structure for table alturban.al_user
DROP TABLE IF EXISTS `al_user`;
CREATE TABLE IF NOT EXISTS `al_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_client` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_client_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `logged_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_user: ~3 rows (approximately)
DELETE FROM `al_user`;
/*!40000 ALTER TABLE `al_user` DISABLE KEYS */;
INSERT INTO `al_user` (`id`, `username`, `auth_key`, `access_token`, `password_hash`, `password_reset_token`, `oauth_client`, `oauth_client_user_id`, `email`, `status`, `created_at`, `updated_at`, `logged_at`) VALUES
	(1, 'webmaster', 'Xd_jbUeM8gKRyuvUoWWxWXKSUv0lMoIX', 'IPMwTROnWhVhRmDmKQq2ffcAp9v1ikrhytFcE1P2', '$2y$13$2GRkxGw16dr0mkfBMm7vJOcZq3erHG7qcab2Fw3xxGqDs1eLQYepi', NULL, NULL, NULL, 'webmaster@example.com', 1, 1466407666, 1466407703, 1466407703),
	(2, 'manager', 'kxQDy2hKWV0nVPhn2PXCToESIwrezjK5', 'sUUaKnlCUBhdWOkeyApu5c7pTZI_LRClGEjz0pww', '$2y$13$P4UfdFZyko7j8bgJkemWRuIf4G6lMh950UpG5S.nL0VMBVhlNBsmO', NULL, NULL, NULL, 'manager@example.com', 1, 1466407666, 1466407666, NULL),
	(3, 'user', 'tsxMvCLduTR3sQUuj3ITlaDSwlE3NCFb', 'iK67eoFkZ09NrUkgcaLYosBMo5SDB0BU9NHdJod9', '$2y$13$h5kPirEeEFmLOySF9BNJ4.bGK.JPxp0PFPiBt4shwq.mFdqo31r1a', NULL, NULL, NULL, 'user@example.com', 1, 1466407667, 1466407667, NULL);
/*!40000 ALTER TABLE `al_user` ENABLE KEYS */;


-- Dumping structure for table alturban.al_user_profile
DROP TABLE IF EXISTS `al_user_profile`;
CREATE TABLE IF NOT EXISTS `al_user_profile` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_base_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `gender` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `al_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_user_profile: ~3 rows (approximately)
DELETE FROM `al_user_profile`;
/*!40000 ALTER TABLE `al_user_profile` DISABLE KEYS */;
INSERT INTO `al_user_profile` (`user_id`, `firstname`, `middlename`, `lastname`, `avatar_path`, `avatar_base_url`, `locale`, `gender`) VALUES
	(1, 'John', NULL, 'Doe', NULL, NULL, 'en-US', NULL),
	(2, NULL, NULL, NULL, NULL, NULL, 'en-US', NULL),
	(3, NULL, NULL, NULL, NULL, NULL, 'en-US', NULL);
/*!40000 ALTER TABLE `al_user_profile` ENABLE KEYS */;


-- Dumping structure for table alturban.al_widget_carousel
DROP TABLE IF EXISTS `al_widget_carousel`;
CREATE TABLE IF NOT EXISTS `al_widget_carousel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_widget_carousel: ~1 rows (approximately)
DELETE FROM `al_widget_carousel`;
/*!40000 ALTER TABLE `al_widget_carousel` DISABLE KEYS */;
INSERT INTO `al_widget_carousel` (`id`, `key`, `status`) VALUES
	(1, 'index', 1);
/*!40000 ALTER TABLE `al_widget_carousel` ENABLE KEYS */;


-- Dumping structure for table alturban.al_widget_carousel_item
DROP TABLE IF EXISTS `al_widget_carousel_item`;
CREATE TABLE IF NOT EXISTS `al_widget_carousel_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carousel_id` int(11) NOT NULL,
  `base_url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `order` int(11) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_carousel` (`carousel_id`),
  CONSTRAINT `fk_item_carousel` FOREIGN KEY (`carousel_id`) REFERENCES `al_widget_carousel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_widget_carousel_item: ~1 rows (approximately)
DELETE FROM `al_widget_carousel_item`;
/*!40000 ALTER TABLE `al_widget_carousel_item` DISABLE KEYS */;
INSERT INTO `al_widget_carousel_item` (`id`, `carousel_id`, `base_url`, `path`, `type`, `url`, `caption`, `status`, `order`, `created_at`, `updated_at`) VALUES
	(1, 1, 'http://localhost/alturban/frontend/web', 'img/yii2-starter-kit.gif', 'image/gif', '/', NULL, 1, 0, NULL, NULL);
/*!40000 ALTER TABLE `al_widget_carousel_item` ENABLE KEYS */;


-- Dumping structure for table alturban.al_widget_menu
DROP TABLE IF EXISTS `al_widget_menu`;
CREATE TABLE IF NOT EXISTS `al_widget_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_widget_menu: ~1 rows (approximately)
DELETE FROM `al_widget_menu`;
/*!40000 ALTER TABLE `al_widget_menu` DISABLE KEYS */;
INSERT INTO `al_widget_menu` (`id`, `key`, `title`, `items`, `status`) VALUES
	(1, 'frontend-index', 'Frontend index menu', '[\n    {\n        "label": "Get started with Yii2",\n        "url": "http://www.yiiframework.com",\n        "options": {\n            "tag": "span"\n        },\n        "template": "<a href=\\"{url}\\" class=\\"btn btn-lg btn-success\\">{label}</a>"\n    },\n    {\n        "label": "Yii2 Starter Kit on GitHub",\n        "url": "https://github.com/trntv/yii2-starter-kit",\n        "options": {\n            "tag": "span"\n        },\n        "template": "<a href=\\"{url}\\" class=\\"btn btn-lg btn-primary\\">{label}</a>"\n    },\n    {\n        "label": "Find a bug?",\n        "url": "https://github.com/trntv/yii2-starter-kit/issues",\n        "options": {\n            "tag": "span"\n        },\n        "template": "<a href=\\"{url}\\" class=\\"btn btn-lg btn-danger\\">{label}</a>"\n    }\n]', 1);
/*!40000 ALTER TABLE `al_widget_menu` ENABLE KEYS */;


-- Dumping structure for table alturban.al_widget_text
DROP TABLE IF EXISTS `al_widget_text`;
CREATE TABLE IF NOT EXISTS `al_widget_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_widget_text_key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table alturban.al_widget_text: ~1 rows (approximately)
DELETE FROM `al_widget_text`;
/*!40000 ALTER TABLE `al_widget_text` DISABLE KEYS */;
INSERT INTO `al_widget_text` (`id`, `key`, `title`, `body`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'backend_welcome', 'Welcome to backend', '<p>Welcome to Yii2 Starter Kit Dashboard</p>', 1, 1466407667, 1466407667);
/*!40000 ALTER TABLE `al_widget_text` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
