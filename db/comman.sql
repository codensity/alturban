-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table survey360.article
DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `view` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `thumbnail_base_url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail_path` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `updater_id` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `published_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article_author` (`author_id`),
  KEY `fk_article_updater` (`updater_id`),
  KEY `fk_article_category` (`category_id`),
  CONSTRAINT `fk_article_author` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_article_category` FOREIGN KEY (`category_id`) REFERENCES `article_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_article_updater` FOREIGN KEY (`updater_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.article: ~0 rows (approximately)
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
/*!40000 ALTER TABLE `article` ENABLE KEYS */;


-- Dumping structure for table survey360.article_attachment
DROP TABLE IF EXISTS `article_attachment`;
CREATE TABLE IF NOT EXISTS `article_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `base_url` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article_attachment_article` (`article_id`),
  CONSTRAINT `fk_article_attachment_article` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table survey360.article_attachment: ~0 rows (approximately)
/*!40000 ALTER TABLE `article_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_attachment` ENABLE KEYS */;


-- Dumping structure for table survey360.article_category
DROP TABLE IF EXISTS `article_category`;
CREATE TABLE IF NOT EXISTS `article_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `parent_id` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article_category_section` (`parent_id`),
  CONSTRAINT `fk_article_category_section` FOREIGN KEY (`parent_id`) REFERENCES `article_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.article_category: ~0 rows (approximately)
/*!40000 ALTER TABLE `article_category` DISABLE KEYS */;
INSERT INTO `article_category` (`id`, `slug`, `title`, `body`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'news', 'News', NULL, NULL, 1, 1449900332, NULL);
/*!40000 ALTER TABLE `article_category` ENABLE KEYS */;


-- Dumping structure for table survey360.file_storage_item
DROP TABLE IF EXISTS `file_storage_item`;
CREATE TABLE IF NOT EXISTS `file_storage_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component` varchar(255) NOT NULL,
  `base_url` varchar(1024) NOT NULL,
  `path` varchar(1024) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `upload_ip` varchar(15) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table survey360.file_storage_item: ~0 rows (approximately)
/*!40000 ALTER TABLE `file_storage_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_storage_item` ENABLE KEYS */;


-- Dumping structure for table survey360.i18n_message
DROP TABLE IF EXISTS `i18n_message`;
CREATE TABLE IF NOT EXISTS `i18n_message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `translation` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`,`language`),
  CONSTRAINT `fk_i18n_message_source_message` FOREIGN KEY (`id`) REFERENCES `i18n_source_message` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.i18n_message: ~0 rows (approximately)
/*!40000 ALTER TABLE `i18n_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `i18n_message` ENABLE KEYS */;


-- Dumping structure for table survey360.i18n_source_message
DROP TABLE IF EXISTS `i18n_source_message`;
CREATE TABLE IF NOT EXISTS `i18n_source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.i18n_source_message: ~0 rows (approximately)
/*!40000 ALTER TABLE `i18n_source_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `i18n_source_message` ENABLE KEYS */;


-- Dumping structure for table survey360.key_storage_item
DROP TABLE IF EXISTS `key_storage_item`;
CREATE TABLE IF NOT EXISTS `key_storage_item` (
  `key` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`key`),
  UNIQUE KEY `idx_key_storage_item_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.key_storage_item: ~5 rows (approximately)
/*!40000 ALTER TABLE `key_storage_item` DISABLE KEYS */;
INSERT INTO `key_storage_item` (`key`, `value`, `comment`, `updated_at`, `created_at`) VALUES
	('backend.layout-boxed', '0', NULL, NULL, NULL),
	('backend.layout-collapsed-sidebar', '0', NULL, NULL, NULL),
	('backend.layout-fixed', '0', NULL, NULL, NULL),
	('backend.theme-skin', 'skin-blue', 'skin-blue, skin-black, skin-purple, skin-green, skin-red, skin-yellow', NULL, NULL),
	('frontend.maintenance', 'disabled', 'Set it to "true" to turn on maintenance mode', NULL, NULL);
/*!40000 ALTER TABLE `key_storage_item` ENABLE KEYS */;


-- Dumping structure for table survey360.page
DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `view` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.page: ~0 rows (approximately)
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` (`id`, `slug`, `title`, `body`, `view`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'about', 'About', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', NULL, 1, 1449900332, 1449900332);
/*!40000 ALTER TABLE `page` ENABLE KEYS */;


-- Dumping structure for table survey360.rbac_auth_assignment
DROP TABLE IF EXISTS `rbac_auth_assignment`;
CREATE TABLE IF NOT EXISTS `rbac_auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `rbac_auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.rbac_auth_assignment: ~3 rows (approximately)
/*!40000 ALTER TABLE `rbac_auth_assignment` DISABLE KEYS */;
INSERT INTO `rbac_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
	('administrator', '1', 1449900337),
	('manager', '2', 1449900337),
	('user', '3', 1449900337);
/*!40000 ALTER TABLE `rbac_auth_assignment` ENABLE KEYS */;


-- Dumping structure for table survey360.rbac_auth_item
DROP TABLE IF EXISTS `rbac_auth_item`;
CREATE TABLE IF NOT EXISTS `rbac_auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `rbac_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `rbac_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.rbac_auth_item: ~3 rows (approximately)
/*!40000 ALTER TABLE `rbac_auth_item` DISABLE KEYS */;
INSERT INTO `rbac_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
	('/*', 2, NULL, NULL, NULL, 1449986799, 1449986799),
	('administrator', 1, NULL, NULL, NULL, 1449900337, 1449900337),
	('AllPermission', 2, NULL, NULL, NULL, 1449986818, 1449986818),
	('loginToBackend', 2, NULL, NULL, NULL, 1449900337, 1449900337),
	('manager', 1, NULL, NULL, NULL, 1449900337, 1449900337),
	('user', 1, NULL, NULL, NULL, 1449900337, 1449900337);
/*!40000 ALTER TABLE `rbac_auth_item` ENABLE KEYS */;


-- Dumping structure for table survey360.rbac_auth_item_child
DROP TABLE IF EXISTS `rbac_auth_item_child`;
CREATE TABLE IF NOT EXISTS `rbac_auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `rbac_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rbac_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.rbac_auth_item_child: ~3 rows (approximately)
/*!40000 ALTER TABLE `rbac_auth_item_child` DISABLE KEYS */;
INSERT INTO `rbac_auth_item_child` (`parent`, `child`) VALUES
	('AllPermission', '/*'),
	('administrator', 'AllPermission'),
	('manager', 'loginToBackend'),
	('administrator', 'manager'),
	('manager', 'user');
/*!40000 ALTER TABLE `rbac_auth_item_child` ENABLE KEYS */;


-- Dumping structure for table survey360.rbac_auth_rule
DROP TABLE IF EXISTS `rbac_auth_rule`;
CREATE TABLE IF NOT EXISTS `rbac_auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.rbac_auth_rule: ~0 rows (approximately)
/*!40000 ALTER TABLE `rbac_auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `rbac_auth_rule` ENABLE KEYS */;


-- Dumping structure for table survey360.system_db_migration
DROP TABLE IF EXISTS `system_db_migration`;
CREATE TABLE IF NOT EXISTS `system_db_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table survey360.system_db_migration: ~15 rows (approximately)
/*!40000 ALTER TABLE `system_db_migration` DISABLE KEYS */;
INSERT INTO `system_db_migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1449900279),
	('m140703_123000_user', 1449900314),
	('m140703_123055_log', 1449900315),
	('m140703_123104_page', 1449900315),
	('m140703_123803_article', 1449900319),
	('m140703_123813_rbac', 1449900321),
	('m140709_173306_widget_menu', 1449900321),
	('m140709_173333_widget_text', 1449900322),
	('m140712_123329_widget_carousel', 1449900324),
	('m140805_084745_key_storage_item', 1449900325),
	('m141012_101932_i18n_tables', 1449900328),
	('m150318_213934_file_storage_item', 1449900329),
	('m150414_195800_timeline_event', 1449900329),
	('m150725_192740_seed_data', 1449900332),
	('m150929_074021_article_attachment_order', 1449900333);
/*!40000 ALTER TABLE `system_db_migration` ENABLE KEYS */;


-- Dumping structure for table survey360.system_log
DROP TABLE IF EXISTS `system_log`;
CREATE TABLE IF NOT EXISTS `system_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_time` double DEFAULT NULL,
  `prefix` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `idx_log_level` (`level`),
  KEY `idx_log_category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.system_log: ~2 rows (approximately)
/*!40000 ALTER TABLE `system_log` DISABLE KEYS */;
INSERT INTO `system_log` (`id`, `level`, `category`, `log_time`, `prefix`, `message`) VALUES
	(1, 1, 'yii\\base\\ErrorException:2', 1449900362.3405, '[backend][/survey360/backend/web/sign-in/login]', 'exception \'yii\\base\\ErrorException\' with message \'symlink(): Cannot create symlink, error code(1314)\' in C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\web\\AssetManager.php:519\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleError(\'<span class="nu...\', \'<span class="st...\', \'<span class="st...\', \'<span class="nu...\', \'...\')\n#1 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\web\\AssetManager.php(519): symlink(\'<span class="st...\', \'<span class="st...\')\n#2 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\web\\AssetManager.php(458): yii\\web\\AssetManager->publishDirectory(\'<span class="st...\', \'[]\')\n#3 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\web\\AssetBundle.php(163): yii\\web\\AssetManager->publish(\'<span class="st...\', \'[]\')\n#4 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\web\\AssetManager.php(266): yii\\web\\AssetBundle->publish(Object(yii\\web\\AssetManager))\n#5 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\web\\AssetManager.php(237): yii\\web\\AssetManager->loadBundle(\'<span class="st...\', \'[]\', \'<span class="ke...\')\n#6 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\web\\View.php(284): yii\\web\\AssetManager->getBundle(\'<span class="st...\')\n#7 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\web\\AssetBundle.php(119): yii\\web\\View->registerAssetBundle(\'<span class="st...\')\n#8 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\validators\\RequiredValidator.php(108): yii\\web\\AssetBundle::register(Object(yii\\web\\View))\n#9 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\widgets\\ActiveField.php(699): yii\\validators\\RequiredValidator->clientValidateAttribute(Object(backend\\models\\LoginForm), \'<span class="st...\', Object(yii\\web\\View))\n#10 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\widgets\\ActiveField.php(210): yii\\widgets\\ActiveField->getClientOptions()\n#11 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\widgets\\ActiveField.php(200): yii\\widgets\\ActiveField->begin()\n#12 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2-bootstrap\\ActiveField.php(187): yii\\widgets\\ActiveField->render(\'<span class="ke...\')\n#13 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\widgets\\ActiveField.php(156): yii\\bootstrap\\ActiveField->render()\n#14 C:\\xampp\\htdocs\\survey360\\backend\\views\\sign-in\\login.php(21): yii\\widgets\\ActiveField->__toString()\n#15 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\View.php(325): require(\'C:\\\\xampp\\\\htdocs...\')\n#16 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile(\'<span class="st...\', \'[<span class="s...\')\n#17 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile(\'<span class="st...\', \'[<span class="s...\', Object(backend\\controllers\\SignInController))\n#18 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Controller.php(371): yii\\base\\View->render(\'<span class="st...\', \'[<span class="s...\', Object(backend\\controllers\\SignInController))\n#19 C:\\xampp\\htdocs\\survey360\\backend\\controllers\\SignInController.php(71): yii\\base\\Controller->render(\'<span class="st...\', \'[<span class="s...\')\n#20 [internal function]: backend\\controllers\\SignInController->actionLogin()\n#21 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(\'[<span class="t...\', \'[]\')\n#22 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Controller.php(151): yii\\base\\InlineAction->runWithParams(\'[]\')\n#23 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Module.php(455): yii\\base\\Controller->runAction(\'<span class="st...\', \'[]\')\n#24 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\web\\Application.php(84): yii\\base\\Module->runAction(\'<span class="st...\', \'[]\')\n#25 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#26 C:\\xampp\\htdocs\\survey360\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#27 {main}'),
	(2, 1, 'yii\\i18n\\PhpMessageSource::loadMessages', 1449900508.4209, '[backend][/survey360/backend/web/sign-in/profile]', 'The message file for category \'filekit/widget\' does not exist: C:\\xampp\\htdocs\\survey360\\vendor\\trntv\\yii2-file-kit\\src\\widget/messages/en/filekit/widget.php'),
	(3, 1, 'yii\\base\\ErrorException:2', 1449985361.5506, '[backend][/survey360/backend/web/admin/permission/assign]', 'exception \'yii\\base\\ErrorException\' with message \'Invalid argument supplied for foreach()\' in C:\\xampp\\htdocs\\survey360\\vendor\\mdmsoft\\yii2-admin\\controllers\\PermissionController.php:142\nStack trace:\n#0 C:\\xampp\\htdocs\\survey360\\vendor\\mdmsoft\\yii2-admin\\controllers\\PermissionController.php(142): yii\\base\\ErrorHandler->handleError(2, \'Invalid argumen...\', \'C:\\\\xampp\\\\htdocs...\', 142, Array)\n#1 [internal function]: mdm\\admin\\controllers\\PermissionController->actionAssign()\n#2 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#3 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Controller.php(151): yii\\base\\InlineAction->runWithParams(Array)\n#4 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Module.php(455): yii\\base\\Controller->runAction(\'assign\', Array)\n#5 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\web\\Application.php(84): yii\\base\\Module->runAction(\'admin/permissio...\', Array)\n#6 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#7 C:\\xampp\\htdocs\\survey360\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#8 {main}'),
	(4, 1, 'yii\\base\\ErrorException:2', 1449985362.0971, '[backend][/survey360/backend/web/admin/permission/assign]', 'exception \'yii\\base\\ErrorException\' with message \'Invalid argument supplied for foreach()\' in C:\\xampp\\htdocs\\survey360\\vendor\\mdmsoft\\yii2-admin\\controllers\\PermissionController.php:133\nStack trace:\n#0 C:\\xampp\\htdocs\\survey360\\vendor\\mdmsoft\\yii2-admin\\controllers\\PermissionController.php(133): yii\\base\\ErrorHandler->handleError(2, \'Invalid argumen...\', \'C:\\\\xampp\\\\htdocs...\', 133, Array)\n#1 [internal function]: mdm\\admin\\controllers\\PermissionController->actionAssign()\n#2 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#3 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Controller.php(151): yii\\base\\InlineAction->runWithParams(Array)\n#4 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Module.php(455): yii\\base\\Controller->runAction(\'assign\', Array)\n#5 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\web\\Application.php(84): yii\\base\\Module->runAction(\'admin/permissio...\', Array)\n#6 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#7 C:\\xampp\\htdocs\\survey360\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#8 {main}'),
	(5, 1, 'yii\\base\\InvalidConfigException', 1449985373.5714, '[backend][/survey360/backend/web/admin/menu]', 'exception \'yii\\base\\InvalidConfigException\' with message \'The table does not exist: {{%menu}}\' in C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\db\\ActiveRecord.php:298\nStack trace:\n#0 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\db\\ActiveRecord.php(327): yii\\db\\ActiveRecord::getTableSchema()\n#1 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\data\\ActiveDataProvider.php(178): yii\\db\\ActiveRecord->attributes()\n#2 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\data\\BaseDataProvider.php(211): yii\\data\\ActiveDataProvider->setSort(Array)\n#3 C:\\xampp\\htdocs\\survey360\\vendor\\mdmsoft\\yii2-admin\\models\\searchs\\Menu.php(56): yii\\data\\BaseDataProvider->getSort()\n#4 C:\\xampp\\htdocs\\survey360\\vendor\\mdmsoft\\yii2-admin\\controllers\\MenuController.php(44): mdm\\admin\\models\\searchs\\Menu->search(Array)\n#5 [internal function]: mdm\\admin\\controllers\\MenuController->actionIndex()\n#6 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): call_user_func_array(Array, Array)\n#7 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Controller.php(151): yii\\base\\InlineAction->runWithParams(Array)\n#8 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Module.php(455): yii\\base\\Controller->runAction(\'\', Array)\n#9 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\web\\Application.php(84): yii\\base\\Module->runAction(\'admin/menu\', Array)\n#10 C:\\xampp\\htdocs\\survey360\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#11 C:\\xampp\\htdocs\\survey360\\backend\\web\\index.php(23): yii\\base\\Application->run()\n#12 {main}'),
	(6, 1, 'yii\\i18n\\PhpMessageSource::loadMessages', 1449986869.9359, '[backend][/survey360/backend/web/sign-in/profile]', 'The message file for category \'filekit/widget\' does not exist: C:\\xampp\\htdocs\\survey360\\vendor\\trntv\\yii2-file-kit\\src\\widget/messages/en/filekit/widget.php'),
	(7, 1, 'yii\\base\\ErrorException:1', 1449986889.194, '[backend][/survey360/backend/web/system-information/index]', 'exception \'yii\\base\\ErrorException\' with message \'Class \'COM\' not found\' in C:\\xampp\\htdocs\\survey360\\vendor\\trntv\\probe\\src\\provider\\WindowsProvider.php:276\nStack trace:\n#0 [internal function]: yii\\base\\ErrorHandler->handleFatalError()\n#1 {main}');
/*!40000 ALTER TABLE `system_log` ENABLE KEYS */;


-- Dumping structure for table survey360.system_rbac_migration
DROP TABLE IF EXISTS `system_rbac_migration`;
CREATE TABLE IF NOT EXISTS `system_rbac_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table survey360.system_rbac_migration: ~3 rows (approximately)
/*!40000 ALTER TABLE `system_rbac_migration` DISABLE KEYS */;
INSERT INTO `system_rbac_migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1449900333),
	('m150625_214101_roles', 1449900337),
	('m150625_215624_init_permissions', 1449900338);
/*!40000 ALTER TABLE `system_rbac_migration` ENABLE KEYS */;


-- Dumping structure for table survey360.timeline_event
DROP TABLE IF EXISTS `timeline_event`;
CREATE TABLE IF NOT EXISTS `timeline_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `event` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_created_at` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.timeline_event: ~3 rows (approximately)
/*!40000 ALTER TABLE `timeline_event` DISABLE KEYS */;
INSERT INTO `timeline_event` (`id`, `application`, `category`, `event`, `data`, `created_at`) VALUES
	(1, 'frontend', 'user', 'signup', '{"public_identity":"webmaster","user_id":1,"created_at":1449900329}', 1449900329),
	(2, 'frontend', 'user', 'signup', '{"public_identity":"manager","user_id":2,"created_at":1449900329}', 1449900329),
	(3, 'frontend', 'user', 'signup', '{"public_identity":"user","user_id":3,"created_at":1449900329}', 1449900329);
/*!40000 ALTER TABLE `timeline_event` ENABLE KEYS */;


-- Dumping structure for table survey360.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_client` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_client_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `logged_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.user: ~3 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `auth_key`, `access_token`, `password_hash`, `password_reset_token`, `oauth_client`, `oauth_client_user_id`, `email`, `status`, `created_at`, `updated_at`, `logged_at`) VALUES
	(1, 'webmaster', '8o42KREvHLXVr9y6D4QWmfV3UfMJxTwb', 'HwgL68D0KkeGHRg_f9g_VVv1ryfDlCdudlnnhj5l', '$2y$13$Er/t8jzTLeeUR6UbMTmpL.eZArNq1v4173.qIWdBVvIee3rAohX9K', NULL, NULL, NULL, 'webmaster@example.com', 1, 1449900330, 1449987394, 1449987394),
	(2, 'manager', '3hbxkWJmSogaEurS8BV0QswJFzWotmG7', 'PCuMeA5hh4GJs42PU1Z62ilBzuOayZcLHvxOfRNq', '$2y$13$d0xde/jXRah.UryjFTCo4ek24E7PcyL34kfunCPaERlGi54uORmE2', NULL, NULL, NULL, 'manager@example.com', 1, 1449900331, 1449900445, 1449900444),
	(3, 'user', 'W5UFUW_D2m7HT9toBlbPkjh1IAqM-l-F', 'iahw18yrPs08hIcg0ECl-vSvKkZLQAjhe3Rsg6mU', '$2y$13$RKFcqvbrp43dBgb/m0/fA.6XEBJ0XMal8pcstKXqUC/zG4xkbK0pe', NULL, NULL, NULL, 'user@example.com', 1, 1449900332, 1449900332, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Dumping structure for table survey360.user_profile
DROP TABLE IF EXISTS `user_profile`;
CREATE TABLE IF NOT EXISTS `user_profile` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_base_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `gender` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.user_profile: ~3 rows (approximately)
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
INSERT INTO `user_profile` (`user_id`, `firstname`, `middlename`, `lastname`, `avatar_path`, `avatar_base_url`, `locale`, `gender`) VALUES
	(1, 'John', NULL, 'Doe', NULL, NULL, 'en-US', NULL),
	(2, NULL, NULL, NULL, NULL, NULL, 'en-US', NULL),
	(3, NULL, NULL, NULL, NULL, NULL, 'en-US', NULL);
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;


-- Dumping structure for table survey360.widget_carousel
DROP TABLE IF EXISTS `widget_carousel`;
CREATE TABLE IF NOT EXISTS `widget_carousel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.widget_carousel: ~0 rows (approximately)
/*!40000 ALTER TABLE `widget_carousel` DISABLE KEYS */;
INSERT INTO `widget_carousel` (`id`, `key`, `status`) VALUES
	(1, 'index', 1);
/*!40000 ALTER TABLE `widget_carousel` ENABLE KEYS */;


-- Dumping structure for table survey360.widget_carousel_item
DROP TABLE IF EXISTS `widget_carousel_item`;
CREATE TABLE IF NOT EXISTS `widget_carousel_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carousel_id` int(11) NOT NULL,
  `base_url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `order` int(11) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_carousel` (`carousel_id`),
  CONSTRAINT `fk_item_carousel` FOREIGN KEY (`carousel_id`) REFERENCES `widget_carousel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.widget_carousel_item: ~0 rows (approximately)
/*!40000 ALTER TABLE `widget_carousel_item` DISABLE KEYS */;
INSERT INTO `widget_carousel_item` (`id`, `carousel_id`, `base_url`, `path`, `type`, `url`, `caption`, `status`, `order`, `created_at`, `updated_at`) VALUES
	(1, 1, 'http://localhost/survey360/frontend/web', 'img/yii2-starter-kit.gif', 'image/gif', '/', NULL, 1, 0, NULL, NULL);
/*!40000 ALTER TABLE `widget_carousel_item` ENABLE KEYS */;


-- Dumping structure for table survey360.widget_menu
DROP TABLE IF EXISTS `widget_menu`;
CREATE TABLE IF NOT EXISTS `widget_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.widget_menu: ~0 rows (approximately)
/*!40000 ALTER TABLE `widget_menu` DISABLE KEYS */;
INSERT INTO `widget_menu` (`id`, `key`, `title`, `items`, `status`) VALUES
	(1, 'frontend-index', 'Frontend index menu', '[\n    {\n        "label": "Get started with Yii2",\n        "url": "http://www.yiiframework.com",\n        "options": {\n            "tag": "span"\n        },\n        "template": "<a href=\\"{url}\\" class=\\"btn btn-lg btn-success\\">{label}</a>"\n    },\n    {\n        "label": "Yii2 Starter Kit on GitHub",\n        "url": "https://github.com/trntv/yii2-starter-kit",\n        "options": {\n            "tag": "span"\n        },\n        "template": "<a href=\\"{url}\\" class=\\"btn btn-lg btn-primary\\">{label}</a>"\n    },\n    {\n        "label": "Find a bug?",\n        "url": "https://github.com/trntv/yii2-starter-kit/issues",\n        "options": {\n            "tag": "span"\n        },\n        "template": "<a href=\\"{url}\\" class=\\"btn btn-lg btn-danger\\">{label}</a>"\n    }\n]', 1);
/*!40000 ALTER TABLE `widget_menu` ENABLE KEYS */;


-- Dumping structure for table survey360.widget_text
DROP TABLE IF EXISTS `widget_text`;
CREATE TABLE IF NOT EXISTS `widget_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_widget_text_key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table survey360.widget_text: ~0 rows (approximately)
/*!40000 ALTER TABLE `widget_text` DISABLE KEYS */;
INSERT INTO `widget_text` (`id`, `key`, `title`, `body`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'backend_welcome', 'Welcome to backend', '<p>Welcome to Yii2 Starter Kit Dashboard</p>', 1, 1449900332, 1449900332);
/*!40000 ALTER TABLE `widget_text` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
