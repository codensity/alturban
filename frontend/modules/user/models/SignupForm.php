<?php
namespace frontend\modules\user\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $confirm_password;
    public $firstname;
    public $lastname;
    public $phone_no;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            [['firstname', 'lastname', 'username', 'email', 'password', 'confirm_password', 'phone_no'], 'required'],
            ['username', 'unique',
                'targetClass'=>'\common\models\User',
                'message' => Yii::t('frontend', 'This username has already been taken.')
            ],
            ['username', 'string', 'min' => 2, 'max' => 255],
            [['firstname', 'lastname'], 'string', 'min' => 2, 'max' => 50],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'unique',
                'targetClass'=> '\common\models\User',
                'message' => Yii::t('frontend', 'This email address has already been taken.')
            ],

            ['password', 'string', 'min' => 6],
            ['confirm_password', 'required'],
            ['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>Yii::t('frontend', "Passwords don't match") ],
            ['phone_no', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'firstname'=>Yii::t('frontend', 'Firstname'),
            'lastname'=>Yii::t('frontend', 'Lastname'),
            'username'=>Yii::t('frontend', 'Username'),
            'email'=>Yii::t('frontend', 'E-mail'),
            'password'=>Yii::t('frontend', 'Password'),
            'confirm_password'=>Yii::t('frontend', 'Confirm Password'),
            'phone_no'=>Yii::t('frontend', 'Phone No'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->save();
            $user->afterSignup();
            return $user;
        }

        return null;
    }
}
