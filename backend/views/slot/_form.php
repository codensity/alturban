<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Slot */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="slot-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="col-md-7">
        <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="form-group col-md-12">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <a class="btn btn-info" href="<?php echo Yii::$app->urlManager->createUrl('/slot/index'); ?>">Back</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
