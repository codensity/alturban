<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Slot */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Slot',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Slots'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slot-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
