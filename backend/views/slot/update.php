<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Slot */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Slot',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Slots'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="slot-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
