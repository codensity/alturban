<?php
/**
 * Created by PhpStorm.
 * User: ravikumar
 * Date: 11/10/17
 * Time: 7:50 PM
 */

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>

<div class="col-md-12">
    <div class="col-md-7">
        <canvas id="myChart" width="400" height="300"></canvas>
    </div>

    <div class="col-md-5">
        <canvas id="myPieChart" height="300"></canvas>
    </div>
</div>

<script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Complete", "Active", "Waiting"],
            datasets: [{
                label: 'Complete',
                data: [12, 19, 3],
                backgroundColor: [
                    '#006400',
                    'rgba(54, 162, 235, 0.2)',
                    '#C2D620'
                ],
//                borderColor: [
//                    'rgba(255, 99, 132, 1)',
//                    'rgba(54, 162, 235, 1)',
//                    'rgba(255, 206, 86, 1)'
//                ],
                borderWidth: 1,
                borderRadius: 5
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });



    var canvas = document.getElementById("myPieChart");
    var ctxi = canvas.getContext('2d');

    // Global Options:
//    Chart.defaults.global.defaultFontColor = 'black';
    Chart.defaults.global.defaultFontSize = 14;

    var data = {
        labels: ["Total Earning of this month","Current earning for today","Potential earning for today"],
        datasets: [
            {
                fill: true,
                backgroundColor: [
                    'blue',
                    'red',
                    'orange',
                ],
                data: [2390, 1450, 580],
// Notice the borderColor
//                borderColor:	['black', 'black'],
//                borderWidth: [2,2]
            }
        ]
    };

    // Notice the rotation from the documentation.

    var options = {
        title: {
            display: true,
//            text: 'What happens when you lend your favorite t-shirt to a girl ?',
            position: 'top'
        },
        rotation: -0.7 * Math.PI
    };


    // Chart declaration:
    var myPieChart = new Chart(ctxi, {
        type: 'pie',
        data: data,
        options: options
    });

    // Fun Fact: I've lost exactly 3 of my favorite T-shirts and 2 hoodies this way :|
</script>
